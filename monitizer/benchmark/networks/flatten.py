import torch

def add_flatten_layer_to_model(name):
    filename = name
    old_model = torch.load(filename)
    modules = [torch.nn.Flatten(1,-1)]
    for layer in old_model:
        modules.append(layer)
        
    new_model = torch.nn.Sequential(*modules)
    torch.save(new_model, filename+"-flatten")

add_flatten_layer_to_model("./networks/cifar_net")