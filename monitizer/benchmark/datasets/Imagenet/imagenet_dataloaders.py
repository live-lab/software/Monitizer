from torchvision import transforms
from monitizer.benchmark.generators.distortiontransformers import To_Tensor_With_Label_Transform, \
    FGSM_Transform
from monitizer.benchmark.generators.distortiondatasets import Restricted_Index_Dataset, Transform_With_Label_Dataset
from monitizer.benchmark.datasets.datasets import get_cifar10_set, get_dtd_set, get_fashionmnist_set, get_kmnist_set, \
    get_svhn_set, get_mnist_set
import torch
from torch.utils.data import DataLoader


