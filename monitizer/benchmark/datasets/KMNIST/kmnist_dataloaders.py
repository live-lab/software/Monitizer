from torchvision import transforms
from monitizer.benchmark.generators.distortiontransformers import To_Tensor_With_Label_Transform, \
    FGSM_Transform
from monitizer.benchmark.generators.distortiondatasets import Transform_With_Label_Dataset
from monitizer.benchmark.datasets.datasets import get_kmnist_set
import torch, os
from torch.utils.data import DataLoader

## Wrong Prediction --------------------------------------------------

def get_kmnist_fgsm_dataloader(usecase, model=None, validation_split=0.2, data_folder='./', root='./',
                              num_workers=os.cpu_count() - 1) -> DataLoader:
    if model == None:
        model = torch.load(root + "/networks/KMNIST")
    transform = None
    kmnist_trainset = get_kmnist_set(usecase, transform, validation_split=validation_split, data_folder=data_folder)
    transform = transforms.Compose(
        [To_Tensor_With_Label_Transform(), FGSM_Transform(model, 0.1)])
    kmnist_trainset = Transform_With_Label_Dataset(kmnist_trainset, transform)
    testloader = DataLoader(kmnist_trainset, batch_size=64, shuffle=False, num_workers=num_workers)
    return testloader
