import torch
from monitizer.benchmark.networks.cifar_10 import Net, Net2


def load_cifar10_network_net1(root):
    model = Net()
    model.load_state_dict(torch.load(root + "networks/cifar_net"))
    return model


def load_cifar10_network(root):
    model = torch.load(root + "networks/CIFAR10-VGG11")
    return model
