# Monitizer

<img src="images/monitizer-logo.png" width="100">

We create a tool (Monitizer) that optimizes monitors on a NN for a specific task.

## Requirements

- Python 3.8 or higher.
- ONNX
- numpy
- pandas
- pytorch
- matplotlib
- onnx2pytorch
- plotly
- kaleido
- tqdm
- tabulate
- scikit-learn

## Installation

Download the repository:

```bash
git clone https://gitlab.com/live-lab/software/monitizer
cd monitizer
```

If you don't have a GPU, run additionally (or check [here](https://pytorch.org/get-started/locally/) for current
version):

```bash
pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cpu
```

And continue with:

```bash
pip install -r requirements.txt 
```

Finally, decide which ID-datasets you will be using (you can also do this again later) and download the datasets by
running:

```bash
./scripts/monitizer --setup <dataset1> ... <datasetN> --data-folder <PATH/TO/DATA>
```

while replacing `<dataset>` with the name of a dataset that you intend to use, e.g. `./monitizer --setup MNIST CIFAR10`
and `<PATH/TO/DATA>` with a folder, where you want the raw data to be stored (default is 'data').
This will automatically download all required datasets, including the respective OOD-datasets, that monitizer needs.

A default command for downloading all implemented datasets is:

```bash
./scripts/monitizer --setup MNIST CIFAR10 CIFAR100 DTD FashionMNIST GTSRB Imagenet KMNIST SVHN --data-folder data
```

Note that Imagenet must be downloaded after logging in, so Monitizer cannot take this over for you.
However, it will tell you, where to download the data and where to put it.
Visit https://www.image-net.org/challenges/LSVRC/2012/2012-downloads.php for downloading ILSVRC2012_devkit_t12.tar.gz,
ILSVRC2012_img_val.tar, and ILSVRC2012_devkit_t12.tar.gz, and put it in your data folder.

### Your first command

Run:

```bash
# assuming you are in the directory Monitizer
./scripts/monitizer --evaluate --monitor-template energy --dataset MNIST --neural-network example-networks/MNIST3x100 --optimize --optimization-objective optimization-objective.ini
```

It optimizes an Energy monitor [(2)](https://link.springer.com/chapter/10.1007/978-3-030-88494-9_14) on a network
called "MNIST3x100", trained on the MNIST dataset. The optimization is random and the resulting monitor is then to be
evaluated.

## Overview

<img src="images/Monitizer-Architecture-new.jpeg" width="1000">

### Use Cases

We envision three different types of users of Monitizer

- **The End User**
    - _Context_:  The end user of a monitor, e.g., an engineer in the aviation industry, is interested in the end
      product, not in the intricacies of the underlying monitoring technique.
      She intends to evaluate one or all monitors provided by Monitizer for her custom NN and dataset, and
      wants to come to a conclusion on which one to use.
      She has a NN that needs to be monitored. Additionally, she has her own proprietary ID dataset, e.g., the one on
      which the NN was trained.
      She wants a monitor that is very general, i.e. optimal on average for all classes, or which can detect a given
      type of
      OOD, for which she knows that her NN would not be able to correctly process them.
    - _Usage_: Such a user can obtain a monitor tuned to her needs using Monitizer without much effort.
      Monitizer supports this feature out-of-the-box. It provides various monitors (19 at present) that can be optimized
      for
      a given network.
      In case she wants to use a custom NN or a dataset, she has to provide the NN as pytorch-dump or in onnx-format.
      and add around 50 lines of code to implement the interface for loading her data.
    - _Required Effort_: After providing the interface for her custom dataset, the user only has to trigger the
      execution, which can be done in
      mere seconds. The execution time depends on the quality of hardware, size of the NN, complexity of the chosen
      monitor,
      and size of the dataset.
      This can vary from less than a minute to a couple of hours.

- **The Developer of Monitors**
    - _Context_: This is developer of monitoring techniques, e.g., a researcher working in runtime verification of
      neural
      networks, aims to create novel techniques and assess their performance in comparison to established methods.
    - _Usage_: Such a user can plug their novel monitor into Monitizer and evaluate it.
      Monitizer directly provides most commonly used NNs and datasets for academic evaluation.
    - _Required Effort_: The code for the monitor needs to be in Python an should
      implement the functions specified in the interface for monitors in Monitizer.
      An experienced developer will need less than an hour to implement these standard functions.
      Afterwards, she can trigger the evaluation of her monitoring technique.
      Depending on the chosen NN and dataset the execution time varies between less than a minute to up to an hour.

- **The scholar**
    - _Context_: An expert in monitoring, e.g., an experienced researcher in NN runtime verification, intends to explore
      beyond the current boundaries.
      She might want to adapt an NN monitor to properties other than OOD, or to experiment with custom NN or datasets.
    - _Usage_: Monitizer provides interfaces, examples, and instructions on how to integrate a new NN, dataset, a
      monitor
      or a custom optimization method or objective.
    - _Required Effort_: The required integration effort is a few hours, assuming that a user knows which dataset, NN,
      and monitor to use.

## Usage

```bash
./scripts/monitizer --monitor-template <monitor_template> --neural-network <neural_network> --optimize --optimization-objective <optimization_objective_specification> --evaluation-criteria <evaluation_criteria> --dataset <dataset> 
```

Replace `<monitor_template>`, `<neural_network.`, `<optimization_objective_specification>`, `<evaluation_criteria>`,
and `<dataset>` with your specific inputs.

For example:

1. **The developer of monitors** will implement her monitor by inheriting from the BaseMonitor-class (see a description
   below) and
   thren run

```bash
./scripts/monitizer --evaluate --monitor-by-user PATH/TO/HER/MONITOR --dataset MNIST --neural-network example-networks/MNIST3x100 --optimize --optimization-objective optimization-objective.ini
```

2. **The end user** will have to implement loading functions for her dataset first (see below) and then run

```bash
./scripts/monitizer --evaluate --monitor-template gauss --dataset HER-DATASET-NAME --neural-network PATH/TO/HER/NN --optimize --optimization-objective optimization-objective.ini
```

3. **The scholar** will (tell her students to) implement her own objective function, dataset, and add another monitor.
   She does so as described below, and changes the configuration file (as described below) and then run:

```bash
./scripts/monitizer --evaluate --monitor-by-user PATH/TO/HER/MONITOR --dataset HER-DATASET-NAME --neural-network PATH/TO/HER/NN --optimize --optimization-objective optimization-objective.ini
```

## Parameters and their meaning:

### Necessary parameters

- `--dataset <dataset_name>` (`-d`): define the dataset. An example is `-d MNIST`
- `--neural-network <network_file>` (`-nn`): define the file where the neural network is stored. This can either be
  pickle-dump (or torch.save) of a pytorch-model or an onnx-file. An example is `-nn example-models/MNIST3x100`

and one of

- `--monitor-template <monitor_template>` (`-m`) : use one of the pre-implemented monitors of Monitizer. This is `gauss`
  for the Gaussian Monitor [1], `energy` for the Energy Monitor [2], `odin` for ODIN [3], or `box` for the Box
  Monitor [4]. An example is `-m gauss`
- `--monitor-by-user <monitor_file>` (`-mu`): use your own monitor-implementation, i.e., provide a python file that
  implements the interface of the [BaseMonitor](monitizer/monitors/Monitor.py). You can additionally
  set: `--monitor-by-user-name <monitor_name>` (`-mu-name`) to specify the name of the class where your monitor is
  implemented (default is "Monitor").
  An example is `-mu path\to\my\monitor\MyMonitor.py -mu-name FancyMonitor`

### Optional parameters

- `--optimize` (`-op`): whether to optimize
- `--parameters` (`-p`): describes the parameters of a monitor. Suppose you already found your optimal parameters, and
  you just want to evaluate, you can use this to provide the tool with the parameters to evaluate on. An example for the
  Energy-Monitor is `-p '{"threshold":60}'`
- `--evaluate` (`-e`): whether to evaluate (boolean option)
- `--optimization-objective` (`-oo`): a config-file (.ini) that contains the relevant information for the objective. An
  example is `-oo optimization-objective.ini`
- `--evaluation-criteria` (`-ec`): specify whether to have a full (`-ec full`) or short evaluation for
  testing (`-ec short`), and whether to compute the AUROC (`-ec auroc`)
- `--evaluation-datasets` (`-ed`): specify the datasets on which to evaluate (e.g. `-ed Rotate Invert`)
- `--confidence-intervals` (`-c`): whether to additionally comput confidence intervals for the evaluated value (e.g.
  AUROC)
- `--output` (`-o`): specifies the location of the output (i.e. the tables of the evaluation). `-o results\final` would
  create the files `results\final-id.csv` and `results\final-ood.csv`
- `--output-latex` (`-l`): will additionally provide latex-code of the csv-files (boolean option)

### Optimization Config

The configuration file for the optimization looks like this:

```python
[Optimization]
[Optimization.Objective]
type = single - objective
function = OptimalForOODClassSubjectToFNR
file =
# single-objective currently contains: OptimalForOODClass, OptimalForOODClassSubjectToFNR
# multi-objective currently contains: OptimalForOODClasses, OptimalForOODClassesSubjectToFNR

[Optimization.Objective.Specification]
ood_classes = Noise / Gaussian
tnr_minimum = 70

[Optimization.Optimizer]
type = random

[Optimization.Optimizer.Random]
episodes = 1

[Optimization.Optimizer.Grid - search]
grid_count = 4

[Optimization.Optimizer.Gradient - descent]
episodes = 1
learning_rate = 1

[Multi - Objective]
num_splits = 4
```

- `[Optimization.Objective]`:
    - `type`: can be either be `single-objective` or `multi-objective`
    - `function`: the name of the optimization objective function (our implementation contains
      currently `OptimalForOODClass`, `OptimalForOODClassSubjectToFNR` for single-,
      and `OptimalForOODClasses`, `OptimalForOODClassesSubjectToFNR` for multi-objective)
    - `file` [optional]: the path to the file containing a custom impelementation of an objective
- `[Optimization.Objective.Specification]`: is specific to the objective that is used. The objective can use everything
  below this section. In the case of `OptimalForOODClassSubjectToFNR`, we have the relevant `ood_class` (that should be
  optimized) as well as the minimum accuracy on the ID-dataset (`tnr_minimum`).
- `[Optimization.Optimizer]`,`type`: define the optimization function. We already support `random`, `grid-search`
  and `gradient-descent`
- `[Optimization.Optimizer.Random]`, `[Optimization.Optimizer.Grid-search]`, `[Optimization.Optimizer.Gradient-descent]`:
  contains each the necessary parameter for the optimization. The user must only specify one of them that they want to
  use
- `Multi-Objective`: additionally define the number of splits (`num\_splits`) for the pareto front

## Running Tests

`python -m unittest discover -s tests`

## Developer Documentation

### Key files and classes

- monitizer.py is the starting point if you want to start understanding the repository
- parse.py
- optimize.py
- evaluate.py
- Bounds-class
- Monitor-class
- Dataset-class
- Objective-class

#### Bounds-class

Since the monitors contain parameters that don't necessarily have to be real numbers (or even integers), we need a
datastructure to capture this.
The parameters can currently be either:

- a real number (with bounds from [FloatBounds](monitizer/monitors/Bounds.py#L24))
- an integer (with bounds from [IntegerBounds](monitizer/monitors/Bounds.py#L48))
- a list of values (with bounds from [UniqueList](monitizer/monitors/Bounds.py#L67))
- a list of parameters, again of type reel, a.s.o., (with bounds from [ListOfBounds](monitizer/monitors/Bounds.py#85))

Each bound must implement three functions:

- `__init__(self)`: to set necessary inputs (e.g. lower and upper bound for the reels or integers)
- `get_random_draw(self)`: returns a random draw within the bounds of this class
- `get_grid_params(self, count)`: returns a list of `count` many parameters for the grid-search within the bounds

#### Monitor-class

The class [BaseMonitor](monitizer/monitors/Monitor.py) is the interface for a monitor that can be optimized by
Monitizier.
Its functions are:

- `__init__(self, model: NeuralNetwork, data: Dataset)`: this function should set up the basic information about the
  monitor. This contains the dataset as a [Dataset](monitizer/benchmark/dataset.py), and a neural network
  as [NeuralNetwork](monitizer/network/neural_network.py). This function should also already name the parameters of this
  monitor and specify the empty bounds.
- `evaluate(self, input: DataLoader) -> np.ndarray`: evaluates the monitor on the provided data as
  a [torch.DataLoader](https://pytorch.org/docs/stable/data.html#torch.utils.data.DataLoader). This should return
  a [numpy-array](https://numpy.org/doc/stable/reference/generated/numpy.array.html) containing one entry for each input
  from the input. The entry should be either `True` (for OOD) or `False` (for ID)
- `fit(self, *args, **kwargs)`: set up the monitor. Some need to precompute values based on the dataset and or/network.
  This should be done within this function
- `set_parameters(self, parameters: dict)`: this function sets the parameters of the monitor if given a corresponding
  dict. For example, the [EnergyMonitor](monitizer/monitors/EnergyMonitor.py) has one parameter `threshold`. The
  dictionary for this monitor must then look like this: `{"threshold":82}`. And the function will set the parameter
  named in the dictionary to the given value.
- `get_parameters(self) -> dict`: creates a dictionary containing all parameters of this monitor with their current
  values. For example, if this was [EnergyMonitor](monitizer/monitors/EnergyMonitor.py) with its only
  parameter `threshold` set to `100` this function would return `{"threshold":100}`
- `get_parameter_bounds(self) -> dict`: returns for each parameter in the model their bounds
  as [Bounds](monitizer/monitors/Bounds.py). For [EnergyMonitor](monitizer/monitors/EnergyMonitor.py) this could look
  liek this `{"threshold": FloatBounds(0,100)}`

#### Objective-class

The class [Objective](monitizer/optimizers/objective.py) is the interface for optimization objectives.
Its functions are:

- `__init__(self, config: OptimizationConfig)`: this function typically just loads relevant information from the config
  and stores them already in useful variables
- `__call__(self, monitor: BaseMonitor)`: this function evalutes the objective on a monitor

The class [MultiObjective](monitizer/optimizers/multi_objectives.py#L8) extends the Objective-class by four more
functions

- `set_weights(self, weights: tuple)`: sets the weights of the multi-objective function. A multi-objective function
  typically contains several objectives (=mutli). Since there is no trivial way to optimize all of them at once (what
  would this even mean), one has to set a weight for each objective. This could be an equal weight to all of them, which
  would mean that the optimization would try to optimize the average over all objectives.
- `get_num_objectives(self)`: returns the number of sub-objectives of the multi-objective class. This is necessary to
  know because there must be as many weights as there are (sub-)objectives
- `evaluate(self, monitor)`: is used by the `__call__`-method, but returns additionally the values of each (sub-)
  objective on the monitor. This is only necessary for the multi-objective case, since in the single-objective there is
  just one objective and the overall evaluation of the monitor equals the evaluation of this objective
- `get_columns(self)`: is a helper function that creates a list of names for the columns of a pandas-DataFrame. The
  columns represent the currently chosen weights and the values of the objectives. This is necessary to collect the
  results for different evaluations of the multi-objective for different weights.

#### Dataset-class

The class [Dataset](monitizer/benchmark/dataset.py) contains the basic implementation of a dataset.
Its functions are:

- `__init__` which can and should be used to pre-load the dataset from the hard-disk
- `get_ID_train`,`get_ID_train_labels`,`get_ID_val`,`get_ID_val_labels`,`get_ID_test` are used to return the ID
  datasets (split into train, validation and test set)
- `get_OOD_data_speicifc`: will return OOD-data that cannot be automatically generated (e.g. NewWorld)
- `get_OOD_data`: will return OOD-data that can be automatically generated (e.g. noise, perturbation)
- `get_OOD_val`,`get_OOD_test`: just calls `get_OOD_data` to get the validation/test OOD data
- `get_all_subset_names`: return all existing OOD-classes for the datasets

## How to add my own Monitor?

Implement the interface given by the class [BaseMonitor](monitizer/monitors/Monitor.py). See as an example
the [EnergyMonitor](monitizer/monitors/EnergyMonitor.py).
Make sure to implement the bounds of the parameters using the [Bounds](monitizer/monitors/Bounds.py). Your parameters
can either be real numbers (float), integers, a subset from a possible set of values, or a list of any of these.

You can then provide your file as input to Monitizer `-mu path\to\my\monitor\MyMonitor.py`. Make sure to either call
your monitor-class **Monitor**, or to include `-mu-name MyFancyClassName` to specify the name of the class where your
monitor is implemented.

### Example

```bash
./scripts/monitizer --monitor-by-user example/user_defined_monitor.py --monitor-by-user-name MyFancyMonitor --dataset MNIST --neural-network example-networks/MNIST3x100
```

Or if you want to integrate your monitor into the framework, you have to:

1. put your monitor into [`monitizer/monitors`](monitizer/monitors)
2. adapt the function [`parse_monitor_template`](monitizer/parse.py#L18), s.th. it recoginzes your shortcut for your
   monitor
3. change the argparse choices of `--monitor-template` in [`monitizer.py`](monitizer/monitizer.py#L62), s.th. it accepts
   your shortcut

## How to add my own evaluation?

1. add your function to [`evaluate.py`](monitizer/evaluate.py) with your implementation and evaluation
2. change the function [`evaluate`](monitizer/evaluate.py#L13) to jump to your evaluation function on your shortcut
3. change the argument of `--evaluation-criteria` in [`monitizer.py`](monitizer/monitizer.py#L439), s.th. it accepts
   your shortcut

## How to add my own optimization-function? 

1. add your function to [`optimize.py`](monitizer/optimize.py) with your optimization implementation
2. change the function [`parse_optimization_function`](monitizer/parse.py#L327) to import your function

## How to add my own optimization-objective?

Implement the interface of [Objective](monitizer/optimizers/objective.py)
or [MultiObjective](monitizer/optimizers/multi_objectives.py#L8).

You can then either provide your file as input to Monitizer: Specify `file=path/to/you/file` and any other relevant
information for your objective in the optimization-objective-config.
Make sure to call your Objective **UserObjective** as the code expects this as class-name.

### Example

```bash
./scripts/monitizer --optimization-objective example/optimization-objective-multi-user.ini --evaluate --monitor-template energy --dataset MNIST --neural-network example-networks/MNIST3x100 --optimize
```

Otherwise, you can integrate your implementation into Monitizer:

1. add your class to [single_objectives.py](monitizer/optimizers/single_objectives.py)
   resp. [multi_objectives](monitizer/optimizers/multi_objectives.py)
2. change the function [`parse_optimization_objective`](monitizer/parse.py#L267) such that it will link to your class
   when the correct input is given in the config

## Lincense

Apache 2.0

### Maintainer(s)

Stefanie Mohr

### Contributers

Stefanie Mohr

Sudeep Kanav

Martha Grobelna

Muqsit Azeem

Sabine Rieder

# Citations

[1]: https://link.springer.com/chapter/10.1007/978-3-030-88494-9_14

[2]: https://proceedings.neurips.cc/paper/2020/file/f5496252609c43eb8a3d147ab9b9c006-Paper.pdf

[3]: https://arxiv.org/abs/1706.02690

[4]: https://ecai2020.eu/papers/1282_paper.pdf