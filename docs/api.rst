Function Guide
==============
Overview over all functions and classes in Monitizer.

.. autosummary::
   :toctree: generated
   :recursive:

   monitizer
   monitizer.benchmark
   monitizer.benchmark.dataset
   monitizer.benchmark.load
