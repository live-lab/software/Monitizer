.. Monitizer documentation master file, created by
   sphinx-quickstart on Mon Nov  6 12:02:09 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Monitizer documentation
=====================================

Introduction
------------

Monitizer is a tool for ...

We provide a :doc:`User Manual<userman>`, which gives information necessary to use Monitizer and develop monitors.

An additional :doc:`Developer Manual<devman>` is made available for those who are interested in implementing their own features.

.. toctree::
   :maxdepth: 3
   :caption: Table of Contents

   userman
   devman
   api


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`


.. toctree::
   :maxdepth: 2
   :caption: Contents:
