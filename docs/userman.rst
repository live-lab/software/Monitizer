User Manual
===========

This document equips the user with the information necessary to use dtControl and run the various decision tree learning
algorithms implemented in it.

Capabilities
------------
...

Getting Started
----------------

A quick start installation guide is available in the `README <LINK>`_.
In this section, we elaborate a little more on the installation process.

.. _installation:

Installation
^^^^^^^^^^^^^^^^^

Getting ...