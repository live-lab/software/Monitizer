from monitizer.monitors.Monitor import BaseMonitor
from monitizer.optimizers.optimization_config import OptimizationConfig
from monitizer.optimizers.multi_objectives import MultiObjective


class UserObjective(MultiObjective):
    def __init__(self, config: OptimizationConfig):
        super().__init__(config)
        self.ood_classes = config.additional_objective_info['ood_classes'].split(',')
        self.tnr_minimum = config.additional_objective_info.getint('tnr_minimum')

    def get_columns(self):
        columns = [f"weight-{ood_class}" for ood_class in self.ood_classes]
        columns += ["result-id"]
        columns += [f"result-{ood_class}" for ood_class in self.ood_classes]
        return columns

    def get_num_objectives(self):
        return len(self.ood_classes)

    def evaluate(self, monitor: BaseMonitor):
        id_result = monitor.evaluate(monitor.data.get_ID_val())
        id_result = (1 - (sum(id_result) / len(id_result)))
        result = []
        for ood_class in self.ood_classes:
            all_results = monitor.evaluate(monitor.data.get_OOD_val(ood_class))
            result.append(sum(all_results) / len(all_results))
        vals = [self.weights[i] for i in range(len(self.weights))]
        vals += [id_result]
        vals += result
        return vals, result, id_result

    def __call__(self, monitor: BaseMonitor) -> float:
        _, result, id_result = self.evaluate(monitor)
        if id_result * 100 < self.tnr_minimum:
            return sum([result[i] * self.weights[i] for i in range(len(result))]) - 10
        else:
            return sum([result[i] * self.weights[i] for i in range(len(result))])
