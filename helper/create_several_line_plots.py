import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import argparse


def create_several_line_plots(dataframes: [pd.DataFrame], labels_legend: [str], output_file: str):
    """
    Helper function that is not used directly in Monitizer.
    """
    matplotlib.rcParams['mathtext.fontset'] = 'stix'
    matplotlib.rcParams['font.family'] = 'STIXGeneral'
    N = None
    labels = None
    for df in dataframes:
        if not N:
            N = len(df)
            labels = df['data'].tolist()
        if len(df) > N:
            N = len(df)
            labels = df['data'].tolist()
    colors = matplotlib.colormaps['jet']

    fig, axs = plt.subplots(1, N - 1)
    for i in range(N - 1):
        axs[i].set_xlim(i, i + 1)
        axs[i].set_ylim(0, 105)
        if i < N - 2:
            axs[i].set_xticks([i])
            axs[i].set_xticklabels([labels[i]], rotation=45, ha='right')
        else:
            axs[i].set_xticks([i, i + 1])
            axs[i].set_xticklabels([labels[i], labels[i + 1]], rotation=45, ha='right')
        if i > 0 and i < N - 2:
            axs[i].set_yticks([])
        if i == N - 2:
            axs[i].yaxis.tick_right()
            axs[i].yaxis.set_label_position("right")

        for j, df in enumerate(dataframes):
            values = df['TPR'].tolist()
            values[0] = df['TNR'].tolist()[0]
            values = [float(v) for v in values]
            color = colors(1 / (len(dataframes) - 1) * j)
            axs[i].plot([i, i + 1], [values[i], values[i + 1]], color=color, label=labels_legend[j])
            axs[i].fill_between([i, i + 1], [values[i], values[i + 1]], color=color, alpha=0.2)
    plt.subplots_adjust(wspace=0)
    plt.legend()
    plt.savefig(f"{output_file}-parallel-line-plot.png", bbox_inches='tight')

parser = argparse.ArgumentParser(description='Processing inputs.')
parser.add_argument('files', nargs='+')
parser.add_argument('-l', nargs='+')
parser.add_argument('-o', type=str, default='result')

args = parser.parse_args()

dfs = []
for file in args.files:
    try:
        df = pd.read_csv(file)
        dfs.append(df)
    except Exception as e:
        print("ERROR!",e)

if len(dfs)>0:
    create_several_line_plots(dfs, args.l, args.o)