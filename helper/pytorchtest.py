import torch
from torch import nn
import torch.nn.functional as F

class TheModelClass(nn.Module):
    def __init__(self):
        super(TheModelClass, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

activation = {}

def get_activation(name):
    def hook(model, input, output):
        activation[name] = output.detach().clone()
    return hook

import numpy as np
from torchvision import datasets, transforms

m = TheModelClass()
named_layers = dict(m.named_modules())

test_set = datasets.GTSRB('data', download=False, transform=transforms.Compose([transforms.ToTensor(), transforms.Resize((32,32))]))

loader = torch.utils.data.DataLoader(test_set, batch_size=64)


for layer in named_layers.keys():
    named_layers[layer].register_forward_hook(get_activation(layer))

acti_total = {}

for idx, (images, labels) in enumerate(loader):
    r = m(images)
    for layer in named_layers.keys():
        if not layer in acti_total.keys():
            acti_total[layer] = activation[layer]
        else:
            acti_total[layer] = torch.cat([acti_total[layer], activation[layer]])


n = torch.load('example-networks/MNIST3x100')
x = torch.Tensor(np.ones((1,1,28,28))*0.5)

activation = {}
named_layers = dict(n.named_modules())
for layer in named_layers.keys():
    named_layers[layer].register_forward_hook(get_activation(layer))

s = n(x)

print("done")