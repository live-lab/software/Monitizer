import torchvision
import torchvision.transforms as transforms
import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torchvision.datasets import GTSRB, SVHN, FashionMNIST, DTD, MNIST, CIFAR10, CIFAR100, KMNIST

import matplotlib.pyplot as plt
import numpy as np

def Net1(classes=10):
    #model = torchvision.models.mobilenet_v3_large(pretrained=True)
    #model.classifier[3] = nn.Linear(in_features=1280, out_features=43)

    modules = []
    modules.append(nn.Conv2d(3, 100, 5))
    modules.append(nn.ELU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.BatchNorm2d(100))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Conv2d(100, 150, 3))
    modules.append(nn.ELU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.BatchNorm2d(150))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Conv2d(150, 250, 1))
    modules.append(nn.ELU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.BatchNorm2d(250))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Flatten())
    modules.append(nn.Linear(250*3*3, 350))
    modules.append(nn.ELU())
    modules.append(nn.BatchNorm1d(350))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Linear(350, classes))
    model = nn.Sequential(*modules)
    return model

def Net2():
    #model = torchvision.models.mobilenet_v3_large(pretrained=True)
    #model.classifier[3] = nn.Linear(in_features=1280, out_features=43)

    modules = []
    modules.append(nn.Conv2d(1, 100, 5))
    modules.append(nn.ELU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.BatchNorm2d(100))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Conv2d(100, 150, 3))
    modules.append(nn.ELU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.BatchNorm2d(150))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Conv2d(150, 250, 1))
    modules.append(nn.ELU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.BatchNorm2d(250))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Flatten())
    modules.append(nn.Linear(1000, 350))
    modules.append(nn.ELU())
    modules.append(nn.BatchNorm1d(350))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Linear(350, 10))
    model = nn.Sequential(*modules)
    return model

# Create Transforms
transform = transforms.Compose([
    transforms.Resize((32, 32)),
    transforms.ToTensor(),
    transforms.Normalize((0.3403, 0.3121, 0.3214),
                         (0.2724, 0.2608, 0.2669))
])

transform2 = transforms.Compose([
    transforms.Resize((28, 28)),
    transforms.ToTensor(),
])


# Function to display an image
def imshow(img):
    img = img / 2 + 0.5  # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()

for datasetname in [#'SVHN','FashionMNIST','DTD',
		    'CIFAR100']:
    print("############### " + datasetname + " ##########################")
    if datasetname == "GTSRB":
# Create Datasets
        trainset = GTSRB(root='../../data', split='train', transform=transform)
        testset = GTSRB(root='../../data', split='test', transform=transform)
        net = Net1(classes=43)
    elif datasetname == "SVHN":
        trainset = SVHN(root='../../data', split='train', transform=transform)
        testset = SVHN(root='../../data', split='test', transform=transform)
        net = Net1(classes=10)
    elif datasetname == "FashionMNIST":
        trainset = FashionMNIST(root='../../data', train=True, transform=transform2)
        testset = FashionMNIST(root='../../data', train=False, transform=transform2)
        net = Net2()
    elif datasetname == "KMNIST":
        trainset = KMNIST(root='../../data', train=True, transform=transform2)
        testset = KMNIST(root='../../data', train=False, transform=transform2)
        net = Net2()
    elif datasetname == "DTD":
        trainset = DTD(root='../../data', split='train', transform=transform)
        testset = DTD(root='../../data', split='test', transform=transform)
        net = Net1(classes=47)
    elif datasetname == "CIFAR100":
        trainset = CIFAR100(root='../../data', train=True, transform=transform)
        testset = CIFAR100(root='../../data', train=False, transform=transform)
        net = Net1(classes=100)
# Load Datasets
    trainloader = torch.utils.data.DataLoader(
        trainset, batch_size=128, shuffle=True, num_workers=2)
    testloader = torch.utils.data.DataLoader(
        testset, batch_size=128, shuffle=False, num_workers=2)

    # Instantiate the neural network

    net = net.cuda()

    #for img,lab in trainloader:
        #img.cuda()
    #    net(img)
    #    break
    #continue

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.1, momentum=0.9)
    loss_plot = []
    net.load_state_dict(torch.load('checkpoints/checkpoint_549'))
    for epoch in range(550,1000):  # loop over the dataset multiple times
        print("running.....")
        running_loss = 0.0
        correct = 0
        total = 0
        for i, data in enumerate(trainloader, 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data
            inputs = inputs.cuda()
            labels = labels.cuda()
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            _, predicted = torch.max(outputs.data, 1)  # outputs.shape = [128,43]
            loss = criterion(outputs, labels)
            loss_plot.append(loss.detach().cpu().numpy())
            # if(i%100==0):
            #     print("loss ", loss.item())
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            if i % 200 == 199:  # print every 200 mini-batches
                print('[%d, %5d] loss: %.3f accuracy: %.3f' %
                      (epoch + 1, i + 1, running_loss / 200, 100 * correct / total))

                running_loss = 0.0
        if epoch%10==9:
            torch.save(net.state_dict(),f"checkpoints/checkpoint_{epoch}")
    print('Finished Training')
    plt.plot(range(len(loss_plot)), loss_plot, 'r+')
    plt.title("Loss")
    plt.savefig('loss.png')
#    exit()

    #PATH = 'best_model.pt'
    #torch.save(net.state_dict(), PATH)
    #print('model saved')

#    continue

    print('Testing......')

    # dataiter = iter(testloader)
    # images, labels = dataiter.next()
    # model = torch.load(PATH)
    # print images
    # imshow(torchvision.utils.make_grid(images[:16]))
    # print('GroundTruth: ', ' '.join('%5s' % labels[j] for j in range(16)))
    """
    Testing
    """

    model = net
    best = 0
    best_model = None
    for i in range(49,1000,50):#[49,99,149,199,249,299,349,399,449,499]:
        model.load_state_dict(torch.load(f'checkpoints/checkpoint_{i}'))
        model.eval()
        model = model.cuda()
        correct = 0
        total = 0

        with torch.no_grad():
            for data in testloader:
                images, labels = data
                images = images.cuda()
                labels = labels.cuda()
                outputs = model(images)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
        if correct/total>best:
            best_model = model
            best = correct/total
        print(f'Accuracy of the {i}th network on the 10000 test images: {100*correct/total}%')
    torch.save(best_model,f"{datasetname}_best_model")

