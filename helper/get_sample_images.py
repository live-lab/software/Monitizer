from monitizer.benchmark.load import *
from monitizer.benchmark.dataset import Dataset
from configparser import ConfigParser
import matplotlib.pyplot as plt

internal_config = ConfigParser()
internal_config.read('monitizer/config/config.ini')
data_folder = internal_config['DEFAULT']['data_folder']
data: Dataset = load("MNIST", data_folder)
o = data.get_ID_val()
if False:
    count = 0
    for b,l in o:
        for i in b:
            if count==0:
                count +=1
                continue
            plt.imshow(i.permute(1,2,0), cmap='gray_r')
            break
        break
    plt.axis('off')
    plt.savefig("CIFAR10_1.png")
    plt.show()
    exit()

count = 0
for b,l in o:
    for i in b:
        #plt.subplot(4,2, 2*count+1)
        #plt.imshow(i.permute(1,2,0))
        #plt.axis('off')

        count += 1
        if count>=4:
            break
    break

#g = data.get_OOD_val("Noise/Gaussian")
g = data.get_OOD_val("UnseenObject/KMNIST")
#g = data.get_OOD_val("NewWorld/DTD")
count = 0
for b,l in g:
    for i in b:
        if count<=2:
            count +=1
            continue
        plt.imshow(i.permute(1, 2, 0), cmap='gray_r')
        break
    break
plt.axis('off')
plt.savefig("KMNIST_4.png")
plt.show()
exit()
for b,l in g:
    for i in b:
        plt.subplot(4,2, 2*(count+1))
        plt.imshow(i.permute(1,2,0), cmap='gray_r')
        plt.axis('off')
        count += 1
        if count>=4:
            break
    break
#plt.savefig("helper/CIFAR10-Contrast.png")
plt.show()

