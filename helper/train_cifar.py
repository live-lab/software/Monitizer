import torch
import torch.nn as nn
import torch.nn.parallel
import torch.nn.functional as F
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import os
import time
#from vgg import VGG11


def get_net3():
    modules = []
    modules.append(nn.Conv2d(3, 64, kernel_size=(3,3), stride=(1,1), padding=(1,1)))
    modules.append(nn.BatchNorm2d(64))
    modules.append(nn.ReLU(inplace=True))
    modules.append(nn.MaxPool2d(kernel_size=2, stride=2))
    modules.append(nn.Conv2d(64,128, kernel_size=(3,3), stride=(1,1), padding=(1,1)))
    modules.append(nn.BatchNorm2d(128))
    modules.append(nn.ReLU(inplace=True))
    modules.append(nn.MaxPool2d(kernel_size=2, stride=2))
    modules.append(nn.Conv2d(128,256, kernel_size=(3,3), stride=(1,1), padding=(1,1)))
    modules.append(nn.BatchNorm2d(256))
    modules.append(nn.ReLU(inplace=True))
    modules.append(nn.Conv2d(256, 256, kernel_size=(3,3), stride=(1,1), padding=(1,1)))
    modules.append(nn.BatchNorm2d(256))
    modules.append(nn.ReLU(inplace=True))
    modules.append(nn.MaxPool2d(kernel_size=2, stride=2))
    modules.append(nn.Conv2d(256, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)))
    modules.append(nn.BatchNorm2d(512))
    modules.append(nn.ReLU(inplace=True))
    modules.append(nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)))
    modules.append(nn.BatchNorm2d(512))
    modules.append(nn.ReLU(inplace=True))
    modules.append(nn.MaxPool2d(kernel_size=2, stride=2))
    modules.append(nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)))
    modules.append(nn.BatchNorm2d(512))
    modules.append(nn.ReLU(inplace=True))
    modules.append(nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)))
    modules.append(nn.BatchNorm2d(512))
    modules.append(nn.ReLU(inplace=True))
    modules.append(nn.MaxPool2d(kernel_size=2, stride=2))
    modules.append(nn.Flatten())
    modules.append(nn.Linear(in_features=512, out_features=10, bias=True))
    model = nn.Sequential(*modules)
    return model


def get_net4():
    modules = []
    modules.append(nn.Flatten())
    modules.append(nn.Linear(3072,3000))
    modules.append(nn.ReLU())
    modules.append(nn.Linear(3000,1000))
    modules.append(nn.ReLU())
    modules.append(nn.Linear(1000,1000))
    modules.append(nn.ReLU())
    modules.append(nn.Linear(1000,100))
    modules.append(nn.ReLU())
    modules.append(nn.Linear(100,10))
    model = nn.Sequential(*modules)
    return model

def get_net2():
    modules = []
    modules.append(nn.Conv2d(3,8,5))
    modules.append(nn.BatchNorm2d(8))
    modules.append(nn.ReLU())
    modules.append(nn.MaxPool2d(2,2))
    modules.append(nn.Conv2d(8,24,3))
    modules.append(nn.BatchNorm2d(24))
    modules.append(nn.ReLU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.Conv2d(24,48,1))
    modules.append(nn.BatchNorm2d(48))
    modules.append(nn.ReLU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.Flatten())
    modules.append(nn.Linear(48*3*3, 120))
    modules.append(nn.ReLU())
    modules.append(nn.Linear(120,84))
    modules.append(nn.ReLU())
    modules.append(nn.Linear(84,10))
    model = nn.Sequential(*modules)
    return model

class Net2(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 8, 5)
        self.batch1 = nn.BatchNorm2d(8)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(8, 24, 3)
        self.batch2 = nn.BatchNorm2d(24)
        self.conv3 = nn.Conv2d(24, 48, 1)
        self.batch3 = nn.BatchNorm2d(48)
        self.fc1 = nn.Linear(48 * 3 * 3, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.batch1(self.conv1(x))))
        x = self.pool(F.relu(self.batch2(self.conv2(x))))
        x = self.pool(F.relu(self.batch3(self.conv3(x))))
        x = torch.flatten(x, 1) # flatten all dimensions except batch
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

best_prec1 = 0
global lr
lr = 0.05

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res

def train(train_loader, model, criterion, optimizer, epoch):
    """
        Run one train epoch
    """
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()

    # switch to train mode
    model.train()

    end = time.time()
    for i, (input, target) in enumerate(train_loader):

        # measure data loading time
        data_time.update(time.time() - end)
        target = target.cuda(non_blocking=True)
        input = input.cuda(non_blocking=True)

        # compute output
        output = model(input)
        loss = criterion(output, target)

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        output = output.float()
        loss = loss.float()
        # measure accuracy and record loss
        prec1 = accuracy(output.data, target)[0]
        losses.update(loss.item(), input.size(0))
        top1.update(prec1.item(), input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()


def validate(val_loader, model, criterion):
    """
    Run evaluation
    """
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    end = time.time()
    for i, (input, target) in enumerate(val_loader):
        target = target.cuda(non_blocking=True)
        input = input.cuda(non_blocking=True)

        # compute output
        with torch.no_grad():
            output = model(input)
            loss = criterion(output, target)

        output = output.float()
        loss = loss.float()

        # measure accuracy and record loss
        prec1 = accuracy(output.data, target)[0]
        losses.update(loss.item(), input.size(0))
        top1.update(prec1.item(), input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

    print(' * Prec@1 {top1.avg:.3f}'
          .format(top1=top1))

    return top1.avg

def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    """
    Save the training model
    """
    torch.save(state, filename)



def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 2 every 30 epochs"""
    global lr
    lr = lr * (0.5 ** (epoch // 30))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr




def main():
    global best_prec1, lr

    #model = Net2()
    #model = VGG11()
    #model = get_net3()
    #model = get_net2()
    model = get_net4()
    print(model)
    model.cuda()

    cudnn.benchmark = True

    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    train_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10(root='./data', train=True, transform=transforms.Compose([
            transforms.RandomHorizontalFlip(),
            transforms.RandomCrop(32, 4),
            transforms.ToTensor(),
            normalize,
        ]), download=True),
        batch_size=128, shuffle=True,
        num_workers=4, pin_memory=True)

    val_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10(root='./data', train=False, transform=transforms.Compose([
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=128, shuffle=False,
        num_workers=4, pin_memory=True)

    # define loss function (criterion) and pptimizer
    criterion = nn.CrossEntropyLoss()
    criterion = criterion.cuda()

    optimizer = torch.optim.SGD(model.parameters(), lr,
                                momentum=0.9,
                                weight_decay=5e-4)

    for epoch in range(300):
        adjust_learning_rate(optimizer, epoch)

        # train for one epoch
        train(train_loader, model, criterion, optimizer, epoch)

        # evaluate on validation set
        prec1 = validate(val_loader, model, criterion)

        # remember best prec@1 and save checkpoint
        is_best = prec1 > best_prec1
        best_prec1 = max(prec1, best_prec1)
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'best_prec1': best_prec1,
        }, is_best, filename=os.path.join('checkpoints', 'checkpoint_{}.tar'.format(epoch)))

if __name__ == '__main__':
    main()
