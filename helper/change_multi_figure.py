import math
from monitizer.benchmark.dataset import Dataset
from monitizer.monitors.Monitor import BaseMonitor
import pandas as pd
import plotly.express as px
import matplotlib
import matplotlib.pyplot as plt


def show_results_multi_objective(data: pd.DataFrame, output_file : str='multi-objective-pareto-frontier'):
    matplotlib.rcParams['mathtext.fontset'] = 'stix'
    matplotlib.rcParams['font.family'] = 'STIXGeneral'
    output_file += '.png'
    objectives = [i for i in data.columns if i.startswith('weight')]
    num_objectives = len(objectives)
    fig = plt.figure(figsize=(5, 3))
    ax = plt.subplot(111)
    if num_objectives == 2:
        lines = [i for i in data.columns if i.startswith('result')]
        prop_cycle = plt.rcParams['axes.prop_cycle']
        colors = prop_cycle.by_key()['color']
        # draw the pareto frontier
        # the weights give the direction, the resulting value is the length of the line
        for i, r in data.iterrows():
            w1 = r[objectives[0]]
            w2 = r[objectives[1]]
            if not w1 == 0 and not w2 == 0:
                m = r[objectives[0]] / r[objectives[1]]
                x = [i for i in range(100) if m / 100 * i < 1]
                y = [m * i for i in x if m * i < 100]
                ax.plot(x, y, color='gray', linestyle='dashed', alpha=0.2)
                t = ax.text(y[-1] / m, y[-1], f"({w1:4.2f},{w2:4.2f})")
                t.set_alpha(0.2)
        for j,line in enumerate(lines):
            xs = []
            ys = []
            for i, r in data.iterrows():
                value = r[line]*100
                if r[objectives[1]] == 0:
                    xs.append(0)
                    ys.append(value)
                    ax.text(0,value,str(round(value,2)), color=colors[j])
                else:
                    m = r[objectives[0]] / r[objectives[1]]
                    xs.append(value * math.cos(math.atan(m)))
                    ys.append(value * math.sin(math.atan(m)))
                    ax.text(xs[-1],ys[-1],str(round(value,2)), color=colors[j])
            if 'id' in line:
                lab = "ID"
            else:
                lab = line
            ax.plot(xs, ys, label=lab.replace('result-',''), alpha=0.7, color=colors[j])
        #plt.title("Pareto-frontier", fontsize=25)
        plt.xlabel(objectives[1].replace("weight-", ""))
        plt.ylabel(objectives[0].replace("weight-", ""))
        plt.legend(loc='upper right')
        plt.xlim(0, 100)
        plt.ylim(0, 100)
        ax.spines[['right', 'top']].set_visible(False)
        # ax.set_axisbelow(True)
        # ax.yaxis.grid(color='gray', linestyle='dashed')
        # ax.xaxis.grid(color='gray', linestyle='dashed')
        plt.savefig(output_file, bbox_inches="tight", pad_inches=0.2)

if __name__ == "__main__":
    df = pd.read_csv('/home/steffi/Documents/research-projects/project-monitizer/monitizer-artifact/paper_results/output/multi-objective_energy_MNIST_contrast_fmnist_fc.csv')
    show_results_multi_objective(df, "TEST")
    print("done")