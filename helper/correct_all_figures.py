import os
from spider_plot import correct
import argparse

parser = argparse.ArgumentParser(description='Processing inputs.')
parser.add_argument('-l', '--folder_path')

args = parser.parse_args()

names = set()
for root, _, files in os.walk(args.folder_path):
    for file in files:
        if file.endswith('id.csv'):
            #names.append(os.path.join())
            names.add(os.path.join(root, file.replace("-id.csv","").replace("-ood.csv","").replace("-figure.png","").replace(".csv","").replace(".ini","").replace(".log","")))

for name in names:
    if os.path.isfile(name+'-ood.csv') and os.path.isfile(name+'-id.csv'):
        correct(name+'-ood.csv',name+'-id.csv')