import os
import argparse
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt

def create_line_plot(df: pd.DataFrame, output_file: str):
    N = len(df)
    data = df['data'].tolist()
    values = df['TPR'].tolist()
    values = [float(v) for v in values]

    fig, axs = plt.subplots(1, N - 1)
    for i in range(N - 1):
        axs[i].set_xlim(i, i + 1)
        axs[i].set_ylim(0, 105)
        if i < N - 2:
            axs[i].set_xticks([i])
            axs[i].set_xticklabels([data[i]], rotation=45, ha='right')
        else:
            axs[i].set_xticks([i, i + 1])
            axs[i].set_xticklabels([data[i], data[i + 1]], rotation=45, ha='right')
        if i > 0 and i < N - 2:
            axs[i].set_yticks([])
        if i == N - 2:
            axs[i].yaxis.tick_right()
            axs[i].yaxis.set_label_position("right")

        axs[i].plot([i, i + 1], [values[i], values[i + 1]],
                    color=(0.98, 0.52, 0, 1))
        axs[i].fill_between([i, i + 1], [values[i], values[i + 1]], color=(0.98, 0.52, 0, 1), alpha=0.2)
    plt.subplots_adjust(wspace=0)

    plt.savefig(f"{output_file}-parallel-line-plot.png", bbox_inches='tight')
    plt.close(fig)


def correct(OOD_FILE, ID_FILE):
    ood_results = pd.read_csv(OOD_FILE)
    id_results = pd.read_csv(ID_FILE)

    com = pd.DataFrame(ood_results[["data", "TPR"]])
    com.loc[len(com)] = ["ID", id_results["TNR"][0]]
    create_line_plot(com, f"{OOD_FILE.replace('-ood.csv','')}")

parser = argparse.ArgumentParser(description='Processing inputs.')
parser.add_argument('folder_path')

args = parser.parse_args()

names = set()
for root, _, files in os.walk(args.folder_path):
    for file in files:
        if file.endswith('id.csv'):
            #names.append(os.path.join())
            names.add(os.path.join(root, file.replace("-id.csv","").replace("-ood.csv","").replace("-figure.png","").replace(".csv","").replace(".ini","").replace(".log","")))

for name in names:
    if os.path.isfile(name+'-ood.csv') and os.path.isfile(name+'-id.csv'):
        correct(name+'-ood.csv',name+'-id.csv')