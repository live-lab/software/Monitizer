import pandas as pd
import plotly.express as px
import argparse


def correct(OOD_FILE, ID_FILE):
    ood_results = pd.read_csv(OOD_FILE)
    id_results = pd.read_csv(ID_FILE)

    com = pd.DataFrame(ood_results[["data", "TPR"]])
    com.loc[len(com)] = ["ID", id_results["TNR"][0]]
    fig = px.line_polar(com, r='TPR', theta="data", line_close=True, range_r=(0,100))
    fig.update_traces(fill="toself")
    fig.write_image(f"{OOD_FILE.replace('-ood.csv','-figure')}.png")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Processing inputs.')
    parser.add_argument('-o', '--ood-file')
    parser.add_argument('-i', '--id-file')

    args = parser.parse_args()

    OOD_FILE = args.ood_file
    ID_FILE = args.id_file
    correct(OOD_FILE, ID_FILE)
