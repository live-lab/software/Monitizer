import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

def create_line_plot(df: pd.DataFrame, output_file: str):
    matplotlib.rcParams['mathtext.fontset'] = 'stix'
    matplotlib.rcParams['font.family'] = 'STIXGeneral'
    N = len(df)
    data = df['data'].tolist()
    values = df['TPR'].tolist()

    fig, axs = plt.subplots(1, N - 1,figsize=(5,2.5))
    for i in range(N - 1):
        axs[i].set_xlim(i, i + 1)
        axs[i].set_ylim(0, 105)
        if i < N - 2:
            axs[i].set_xticks([i])
            axs[i].set_xticklabels([data[i]], rotation=45, ha='right')
        else:
            axs[i].set_xticks([i, i + 1])
            axs[i].set_xticklabels([data[i], data[i + 1]], rotation=45, ha='right')
        if i > 0 and i < N - 2:
            axs[i].set_yticks([])
        if i == N - 2:
            axs[i].yaxis.tick_right()
            axs[i].yaxis.set_label_position("right")

        axs[i].plot([i, i + 1], [values[i], values[i + 1]],
                    color=(0.98, 0.52, 0, 1))
        axs[i].fill_between([i, i + 1], [values[i], values[i + 1]], color=(0.98, 0.52, 0, 1), alpha=0.2)
    plt.subplots_adjust(wspace=0)

    plt.savefig(f"{output_file}-parallel-line-plot.png", bbox_inches='tight')

if __name__ == "__main__":
    df = pd.read_csv('../../monitizer-artifact/paper_results/output/gauss_CIFAR10_random_30_OptimalForOODClassSubjectToFNR_Perturbation-GaussianBlur-ood.csv')
    df2 = pd.read_csv('../../monitizer-artifact/paper_results/output/gauss_CIFAR10_random_30_OptimalForOODClassSubjectToFNR_Perturbation-GaussianBlur-id.csv')
    com = pd.DataFrame(df[["data", "TPR"]])
    com.loc[len(com)] = ["ID", df2["TNR"][0]]
    create_line_plot(com, '../../paper-repo/draft-tacas-24/figures-eval/gauss_CIFAR10_random_30_OptimalForOODClassSubjectToFNR_Perturbation-GaussianBlur-new')