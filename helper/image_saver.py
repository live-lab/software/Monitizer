from monitizer.benchmark.datasets.MNIST.mnist_dataset import Dataset as mnist_dataset
from monitizer.benchmark.datasets.CIFAR10.cifar10_dataset import Dataset as cifar10_dataset
from torchvision import transforms

# TODO: test and optimize
def store(dataset, distortion, usecase, root="./", max_number_images=200):
    if dataset == "MNIST":
        dataset = mnist_dataset("mnist dataset")
        dataloader = dataset.get_OOD_data(distortion, usecase)
        iterator = iter(dataloader)
        counter = 0
        transform = transforms.ToPILImage()
        while counter < max_number_images:
            data, target = next(iterator, (None, None))
            if data == None:
                break
            else:
                image = transform(data[0])
                image.save(f"{root}ood_data/MNIST/{distortion}/{usecase}/{counter}.png")
                counter+=1
    elif dataset == "CIFAR10":
        dataset = cifar10_dataset("cifar10 dataset")
        dataloader = dataset.get_OOD_data(distortion, usecase)
        iterator = iter(dataloader)
        counter = 0
        transform = transforms.ToPILImage()
        while counter < max_number_images:
            data, target = next(iterator, (None, None))
            if data == None:
                break
            else:
                image = transform(data[0])
                image.save(f"{root}ood_data/CIFAR10/{distortion}/{usecase}/{counter}.png")
                counter+=1
        

store("MNIST", "NewWorld/CIFAR10", "test")