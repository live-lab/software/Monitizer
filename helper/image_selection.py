import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import torch
from monitizer.benchmark.datasets.datasets import get_kmnist, get_fashionmmnist,\
      get_cifar10, get_svhn, get_dtd, \
      get_gtsrb, get_cifar100
from torchvision import transforms
import monitizer.benchmark.utils as utils

def get_fashionmnist_output_label(label):
    output_mapping = {
        0: "T-shirt/Top",
        1: "Trouser",
        2: "Pullover",
        3: "Dress",
        4: "Coat",
        5: "Sandal",
        6: "Shirt",
        7: "Sneaker",
        8: "Bag",
        9: "Ankle Boot"
    }
    input = (label.item() if type(label) == torch.Tensor else label)
    return output_mapping[input]


def get_kmnist_output_label(label):
    return (label.item() if type(label) == torch.Tensor else label)

def get_svhn_output_label(label):
    return (label.item() if type(label) == torch.Tensor else label)

def get_cifar10_output_label(label):
    classes = ('plane', 'car', 'bird', 'cat',
               'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
    index = (label.item() if type(label) == torch.Tensor else label)
    return classes[index]

def get_cifar100_output_label(label):
    classes = ['apple', 'aquarium_fish', 'baby', 'bear', 'beaver', 'bed', 'bee', 'beetle',
               'bicycle', 'bottle', 'bowl', 'boy', 'bridge', 'bus', 'butterfly', 'camel',
               'can', 'castle', 'caterpillar', 'cattle', 'chair', 'chimpanzee', 'clock',
               'cloud', 'cockroach', 'couch', 'crab', 'crocodile', 'cup', 'dinosaur', 'dolphin',
               'elephant', 'flatfish', 'forest', 'fox', 'girl', 'hamster', 'house', 'kangaroo',
               'keyboard', 'lamp', 'lawn_mower', 'leopard', 'lion', 'lizard', 'lobster', 'man',
               'maple_tree', 'motorcycle', 'mountain', 'mouse', 'mushroom', 'oak_tree', 'orange',
               'orchid', 'otter', 'palm_tree', 'pear', 'pickup_truck', 'pine_tree', 'plain', 'plate',
               'poppy', 'porcupine', 'possum', 'rabbit', 'raccoon', 'ray', 'road', 'rocket', 'rose',
               'sea', 'seal', 'shark', 'shrew', 'skunk', 'skyscraper', 'snail', 'snake', 'spider', 'squirrel',
               'streetcar', 'gtsrbflower', 'sweet_pepper', 'table', 'tank', 'telephone', 'television', 'tiger',
               'tractor', 'train', 'trout', 'tulip', 'turtle', 'wardrobe', 'whale', 'willow_tree', 'wolf',
               'woman', 'worm']
    index = (label.item() if type(label) == torch.Tensor else label)
    return classes[index]


def get_dtd_output_label(label):
    return (label.item() if type(label) == torch.Tensor else label)

def get_gtsrb_output_label(label):
    classes = ['20_speed', '30_speed', '50_speed', '60_speed', '70_speed', '80_speed',
               '80_lifted', '100_speed', '120_speed', 'no_overtaking_general', 'no_overtaking_trucks', 
               'right_of_way_crossing', 'right_of_way_general', 'give_way', 'stop', 'no_way_general', 
               'no_way_trucks', 'no_way_one_way', 'attention_general', 'attention_left_turn', 'attention_right_turn',
               'attention_curvy', 'attention_bumpers', 'attention_slippery', 'attention_bottleneck', 
               'attention_construction', 'attention_traffic_light', 'attention_pedestrian', 'attention_children', 
               'attention_bikes', 'attention_snowflake', 'attention_deer', 'lifted_general', 'turn_right', 
               'turn_left', 'turn_straight', 'turn_straight_right', 'turn_straight_left', 'turn_right_down', 
               'turn_left_down', 'turn_circle', 'lifted_no_overtaking_general', 'lifted_no_overtaking_trucks']
    index = (label.item() if type(label) == torch.Tensor else label)
    return classes[index]

def get_mnist_output_label(label):
    return (label.item() if type(label) == torch.Tensor else label)


mnist_ood_datasets = ["kmnist", "fashionmnist", "svhn", "dtd", "cifar10"]
cifar10_ood_datasets = ["cifar100", "dtd", "gtsrb"]
cifar100_ood_datasets = ["cifar10", "gtsrb"]


def create_list_of_images(iterator, model, label_function_model, label_function_data,
                          output_file, ignored_labels=[], gray=False):
    if gray:
        cmap="gray"
    else:
        cmap="viridis"
    transform = transforms.ToPILImage()
    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.2)
    fig.add_subplot(1 ,1, 1)
    data, label = next(iterator)
    index=-1
    while label_function_data(label) in ignored_labels:
        index+=1
        data, label = next(iterator)
    data_pil = transform(data[0])
    axes_image = plt.imshow(data_pil,cmap=cmap)
    pred_label = None
    if model != None: 
        pred_label = label_function_model(model(data).argmax())
    fig.get_axes()[0].set_title(f"Label: {label_function_data(label)} <-> Prediction: \
                                {pred_label}")


    class Index:

        def __init__(self, index=-1) -> None:
            self.ind = index
            self.accepted_indices = []

        def update_plot(self):
            data, label = next(iterator)
            while label_function_data(label) in ignored_labels:
                data, label = next(iterator)
                self.ind += 1
            data_pil = transform(data[0])
            axes_image.set_data(data_pil)
            pred_label = None
            if model != None: 
                pred_label = label_function_model(model(data).argmax())
            fig.get_axes()[0].set_title(f"Label: {label_function_data(label)} <-> Prediction: \
                                {pred_label}")
            plt.draw()


        def accept(self, event):
            self.ind += 1
            self.accepted_indices.append(self.ind)
            self.update_plot()
            

        def reject(self, event):
            self.ind += 1
            self.update_plot()


    callback = Index(index)
    axrej= fig.add_axes([0.7, 0.05, 0.1, 0.075])
    axacc= fig.add_axes([0.81, 0.05, 0.1, 0.075])
    baccept = Button(axacc, 'Accept')
    baccept.on_clicked(callback.accept)
    breject = Button(axrej, 'Reject')
    breject.on_clicked(callback.reject)

    plt.show()

    with open(output_file, 'w+') as f:
        f.write(str(callback.accepted_indices))


def create_list_of_images_automatic(iterator, model, label_function_model, label_function_data,
                          output_file, ignored_labels=[], gray=False):
    number_images_required = 100
    index = 0
    accepted_indices = []
    while len(accepted_indices) < number_images_required:
        data, labels = next(iterator)
        for i, label in enumerate(labels):
            if label_function_data(label) in ignored_labels:
                index+=1
                continue
            accepted_indices.append(index)
            index += 1
            if len(accepted_indices) >= number_images_required:
                break
    with open(output_file, 'w+') as f:
        f.write(str(accepted_indices))
        


def mnist_selection(dataset, usecase, validation_split=0.2, model_file="./networks/MNIST4x350-flatten", root="./"):
    model = torch.load(model_file)

    if dataset=="kmnist":
        transform=transforms.ToTensor()
        output_label = get_kmnist_output_label
        data_loader = get_kmnist(usecase, transform, validation_split, root)
    elif dataset=="fashionmnist":
        transform=transforms.ToTensor()
        output_label = get_fashionmnist_output_label
        data_loader = get_fashionmmnist(usecase, transform, validation_split, root)
    elif dataset=="svhn":
        transform=transforms.Compose([transforms.ToTensor(), transforms.Resize(size=(28,28)), transforms.Grayscale(1)])
        output_label = get_svhn_output_label
        data_loader = get_svhn(usecase, transform, root)
    elif dataset=="dtd":
        transform=transforms.Compose([transforms.ToTensor(), transforms.Resize(size=(28,28)), transforms.Grayscale(1)])
        output_label = get_dtd_output_label
        data_loader = get_dtd(usecase, transform, root)
    elif dataset=="cifar10":
        transform=transforms.Compose([transforms.ToTensor(), transforms.Resize(size=(28,28)), transforms.Grayscale(1)])
        output_label = get_cifar10_output_label
        data_loader = get_cifar10(usecase, transform, validation_split, root)
    iterator = iter(data_loader)
    output_path = root + f"specifications/MNIST/{dataset}_list_{usecase}.txt"
    create_list_of_images(iterator, model, get_mnist_output_label, output_label, output_path, gray=True)


def mnist_selection_all(validation_split=0.2, model_file="./networks/MNIST4x350-flatten", root="./"):
    for set in mnist_ood_datasets:
        for type in ["train", "val", "test"]:
            mnist_selection(set, type, validation_split, model_file, root)



def cifar10_selection(dataset, usecase, validation_split=0.2, root="./"):
    model = utils.load_cifar10_network_net1(root)

    if dataset=="cifar100":
        transform=transforms.ToTensor()
        output_label = get_cifar100_output_label
        data_loader = get_cifar100(usecase, transform, validation_split, root)
        ignored_labels = ['pickup_truck', 'streetcar', 'wolf']
    elif dataset=="dtd":
        transform=transforms.Compose([transforms.ToTensor(), transforms.Resize(size=(32,32))])
        output_label = get_dtd_output_label
        data_loader = get_dtd(usecase, transform, root)
        ignored_labels = []
    elif dataset=="gtsrb":
        transform=transforms.Compose([transforms.ToTensor(), transforms.Resize(size=(32,32))])
        output_label = get_gtsrb_output_label
        data_loader = get_gtsrb(usecase, transform, root=root)
        ignored_labels = []
    iterator = iter(data_loader)
    output_path = root + f"specifications/cifar10/{dataset}_list_{usecase}.txt"
    create_list_of_images(iterator, model, get_cifar10_output_label, output_label, output_path, ignored_labels)


def cifar10_selection_all(validation_split=0.2, root="./"):
    for set in cifar10_ood_datasets:
        for type in ["train", "val", "test"]:
            cifar10_selection(set, type, validation_split, root)

def cifar100_selection(dataset, usecase, validation_split=0.2, root="./"):
    model = None

    if dataset=="cifar10":
        transform=transforms.ToTensor()
        output_label = get_cifar10_output_label
        data_loader = get_cifar10(usecase, transform, validation_split, root)
        ignored_labels = ['automobile', 'truck',]
    # elif dataset=="dtd":
    #     transform=transforms.Compose([transforms.ToTensor(), transforms.Resize(size=(32,32))])
    #     output_label = get_dtd_output_label
    #     data_loader = get_dtd(usecase, transform, root)
    #     ignored_labels = []
    elif dataset=="gtsrb":
        transform=transforms.Compose([transforms.ToTensor(), transforms.Resize(size=(32,32))])
        output_label = get_gtsrb_output_label
        data_loader = get_gtsrb(usecase, transform, data_folder=root)
        ignored_labels = ["no_way_trucks"]
    iterator = iter(data_loader)
    output_path = root + f"monitizer/benchmark/specifications/cifar100/{dataset}_list_{usecase}.txt"
    create_list_of_images_automatic(iterator, model, get_cifar100_output_label, output_label, output_path, ignored_labels)


def cifar100_selection_all(validation_split=0.2, root="./"):
    for set in cifar100_ood_datasets:
        for type in ["train", "val", "test"]:
            cifar100_selection(set, type, validation_split, root)

cifar100_selection_all()

