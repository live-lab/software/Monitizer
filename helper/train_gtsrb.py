import sys
sys.path.insert(0,'.')

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.nn.functional as F
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import os
import time
#from vgg import VGG11
from torchvision.models import vgg11
from monitizer.benchmark.load import load
import torchvision

def get_net3():
    #model = torchvision.models.mobilenet_v3_large(pretrained=True)
    #model.classifier[3] = nn.Linear(in_features=1280, out_features=43)

    modules = []
    modules.append(nn.Conv2d(3, 100, 5))
    modules.append(nn.ELU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.BatchNorm2d(100))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Conv2d(100, 150, 3))
    modules.append(nn.ELU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.BatchNorm2d(150))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Conv2d(150, 250, 1))
    modules.append(nn.ELU())
    modules.append(nn.MaxPool2d(2, 2))
    modules.append(nn.BatchNorm2d(250))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Flatten())
    modules.append(nn.Linear(250*3*3, 350))
    modules.append(nn.ELU())
    modules.append(nn.BatchNorm1d(350))
    modules.append(nn.Dropout(p=0.5))

    modules.append(nn.Linear(350, 43))
    model = nn.Sequential(*modules)
    return model

def gets():
    modules = []
    # Localization
    modules.append(nn.Conv2d(3, 8, kernel_size=7))
    modules.append(nn.MaxPool2d(2, stride=2))
    modules.append(nn.ReLU(True))
    modules.append(nn.Conv2d(8, 10, kernel_size=5))
    modules.append(nn.MaxPool2d(2, stride=2))
    modules.append(nn.ReLU(True))


    modules.append(nn.Conv2d(3, 100, kernel_size=5))
    modules.append(nn.LeakyReLU())
    modules.append(nn.MaxPool2d(2))
    modules.append(nn.BatchNorm2d(100))
    modules.append(nn.Dropout2d())

    modules.append(nn.Conv2d(100, 150, kernel_size=3))
    modules.append(nn.LeakyReLU())
    modules.append(nn.MaxPool2d(2))
    modules.append(nn.BatchNorm2d(150))
    modules.append(nn.Dropout2d())

    modules.append(nn.Conv2d(150, 250, kernel_size=3))
    modules.append(nn.LeakyReLU())
    modules.append(nn.MaxPool2d(2))
    modules.append(nn.BatchNorm2d(250))
    modules.append(nn.Dropout2d())

    modules.append(nn.Flatten())
    modules.append(nn.Linear(250 * 2 * 2, 350))
    modules.append(nn.ReLU())
    modules.append(nn.Dropout())
    modules.append(nn.Linear(350, 43))

    model = nn.Sequential(*modules)
    return model


best_prec1 = 0
global lr
lr = 0.1

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res

def train(train_loader, model, criterion, optimizer, epoch):
    """
        Run one train epoch
    """
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()

    # switch to train mode
    model.train()

    end = time.time()
    for i, (input, target) in enumerate(train_loader):

        # measure data loading time
        data_time.update(time.time() - end)
        target = target.cuda(non_blocking=True)
        input = input.cuda(non_blocking=True)

        # compute output
        output = model(input)
        loss = criterion(output, target)

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        output = output.float()
        loss = loss.float()
        # measure accuracy and record loss
        prec1 = accuracy(output.data, target)[0]
        losses.update(loss.item(), input.size(0))
        top1.update(prec1.item(), input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()


def validate(val_loader, model, criterion):
    """
    Run evaluation
    """
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    end = time.time()
    for i, (input, target) in enumerate(val_loader):
        target = target.cuda(non_blocking=True)
        input = input.cuda(non_blocking=True)

        # compute output
        with torch.no_grad():
            output = model(input)
            loss = criterion(output, target)

        output = output.float()
        loss = loss.float()

        # measure accuracy and record loss
        prec1 = accuracy(output.data, target)[0]
        losses.update(loss.item(), input.size(0))
        top1.update(prec1.item(), input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

    print(' * Prec@1 {top1.avg:.3f}'
          .format(top1=top1))

    return top1.avg

def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
    """
    Save the training model
    """
    torch.save(state, filename)



def adjust_learning_rate(optimizer, epoch):
    """Sets the learning rate to the initial LR decayed by 2 every 30 epochs"""
    global lr
    lr = lr * (0.5 ** (epoch // 30))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr




def main():
    global best_prec1, lr

    model = get_net3()
    #model = vgg11()
    print(model)
    model.cuda()

    cudnn.benchmark = True

    #normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
    #                                 std=[0.229, 0.224, 0.225])

    GTSRB = load('gtsrb','../data',num_workers=4)
    train_loader = GTSRB.get_ID_train()
    val_loader = GTSRB.get_ID_val()

    for images, labels in train_loader:
        images = images.cuda()
        res = model(images)
        break

    # define loss function (criterion) and pptimizer
    criterion = nn.CrossEntropyLoss()
    criterion = criterion.cuda()

    optimizer = torch.optim.SGD(model.parameters(), 0.001,
                                momentum=0.9,
                                #weight_decay=5e-4
    for epoch in range(30):
        adjust_learning_rate(optimizer, epoch)

        # train for one epoch
        train(train_loader, model, criterion, optimizer, epoch)

        # evaluate on validation set
        prec1 = validate(val_loader, model, criterion)

        # remember best prec@1 and save checkpoint
        is_best = prec1 > best_prec1
        best_prec1 = max(prec1, best_prec1)
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'best_prec1': best_prec1,
        }, is_best, filename=os.path.join('checkpoints', 'checkpoint_{}.tar'.format(epoch)))

if __name__ == '__main__':
    main()
