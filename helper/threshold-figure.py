import matplotlib.pyplot as plt
import numpy as np
import math
import matplotlib

def p(x,m=0,s=2):
    return np.exp(-(x-m)**2/(2*(s**2)))/(s*np.sqrt(2*math.pi))
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'
fix, ax = plt.subplots()
x = np.linspace(-2.8,7,200)
y1 = p(x,m=3,s=1.4)+0.5
y2 = p(x,m=-0.9,s=0.4)+0.5
ax.plot(x,y1, color='blue', alpha=.3)
ax.plot(x,y2, color='orange', alpha=.3)
plt.xlim(-3,7)
plt.ylim(0,1.6)
ax.fill_between(x,y1,0.5, color='blue', alpha=.3)
ax.fill_between(x,y2,0.5, color='orange', alpha=.3)
ax.spines[['right', 'top','bottom','left']].set_visible(False)
ax.set_xticks([])
ax.set_yticks([])
ax.plot(1, 0.5, ls="", marker=">", ms=10, color="k",
            transform=ax.get_yaxis_transform(), clip_on=False)
ax.plot(-2.8,1, ls="", marker="^", ms=10, color="k",
            transform=ax.get_xaxis_transform(), clip_on=False)
ax.text(-1.5,0.7,"OOD", fontsize='xx-large')
ax.text(2.7,0.6,"ID",fontsize='xx-large')
#ax.spines['bottom'].set_linewidth(4)
#ax.spines['left'].set_linewidth(4)

#ax.arrow(0,-1,0,0.5,width=0.1,edgecolor='black',
#         facecolor='black', head_length=0.1)
plt.arrow(-2.8,0.5,0,1.2)
plt.arrow(-2.8,0.5,10,0)
plt.arrow(0.1,0.3,0,0.2)
ax.plot(0.1, 0.29, ls="", marker="^", ms=10, color="k",
            transform=ax.get_xaxis_transform(), clip_on=False)
ax.text(-0.5,0.2,"threshold τ", fontsize='xx-large')
plt.savefig("threshold-demo.png", bbox_inches='tight')
plt.show()
