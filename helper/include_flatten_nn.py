import torch

filename = "FILE"
old_model = torch.load(filename)
modules = [torch.nn.Flatten(1,-1)]
for layer in old_model:
    modules.append(layer)
    
new_model = torch.nn.Sequential(*modules)
torch.save(new_model, filename+"-flatten")
