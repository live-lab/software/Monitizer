import os.path

import pandas as pd
import plotly.express as px
import argparse
import plotly.graph_objects as go

def make_plot(files):
    com = None
    fig = go.Figure()
    for file in files:
        monitor = os.path.basename(file).split('_')[2]
        results = pd.read_csv(file)
        results.at[0,"TPR"] =  results.at[0,"TNR"]
        if com is None:
            com = pd.DataFrame(results[["data", "TPR"]])
            com["monitor"] = [monitor]*len(com)
        else:
            a = results[["data", "TPR"]]
            a["monitor"] = [monitor]*len(a)
            com = pd.concat([com, a], axis=0)
        r = [float(i) for i in list(results["TPR"])]
        fig.add_trace(go.Scatterpolar(
            r=r+[r[0]],
            theta=results["data"],
            fill='toself',
            name=monitor,
        ))
    fig.update_layout(
        polar=dict(
            radialaxis=dict(
                visible=True,
                range=[0, 100]
            )),
        showlegend=True
    )
    fig.show()
    #fig = px.line_polar(com, r='TPR', theta="data", line_close=True, range_r=(0,100), color='monitor')
    #fig.update_traces(fill="toself")
    fig.write_image(f"combined.png")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Processing inputs.')
    parser.add_argument('--datafiles', nargs='+')

    args = parser.parse_args()

    #for file in args.datafiles:
    #    filename = os.path.basename(file)
    #    root = os.path.dirname(file)



    make_plot(args.datafiles)
