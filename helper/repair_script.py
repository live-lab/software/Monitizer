# For some optimimzations the evaluation was not done because of an error
# this file gets the optimal parameters from the logs and then restarts the evaluation

import os
import sys
import pathlib


def parse_log(logfile):
    params = {}
    with open(logfile,'r') as f:
        lines = f.readlines()
    names = lines[1].replace("Namespace(","").replace(")","").replace('\n','')
    names = names.split(',')
    for n in names:
        m = n.split('=')
        params[m[0].replace(' ','')] = m[1].replace("'","")
    for line in lines:
        if "The optimal parameters" in line:
            params['parameter'] = line[line.find('{'):line.find('}')+1]
    return params

def do_eval(FOLDER):
    files = os.listdir(FOLDER)
    for file in files:
        if file.endswith(".log"):
            params = parse_log(FOLDER+file)
            print(f"\n EVALUATING {os.path.basename(params['output'])}")
            continue
            run_monitizer(monitor_template=params['monitor_template'],
                          monitor_by_user=None,
                          neural_network_file=params['neural_network'],
                          optimization_objective_specs=None,
                          evaluation_criteria='full',
                          dataset_name=params['dataset'],
                          parameter_by_user=params['parameter'],
                          optimize=None,
                          evaluation=True,
                          output_file=FOLDER+os.path.basename(params['output']))

sys.dont_write_bytecode = True  # prevents writing .pyc files

script = pathlib.Path(__file__).resolve()
project_dir = script.parent

sys.path.insert(0, str(project_dir))

import monitizer.monitizer  # noqa E402
from monitizer.monitizer import run_monitizer

FOLDER = "/home/steffi/Documents/research-projects/project-monitizer/monitizer-artifact/my-results/"
FOLDER += "2023-09-06_11-54-29/"
FOLDER += "odin/"
FOLDER += "MNIST/"
FOLDER += "random/"
FOLDER += "OptimalForOODClassSubjectToFNR/"

do_eval(FOLDER)
exit()
FOLDER = "/home/steffi/Documents/research-projects/project-monitizer/monitizer-artifact/my-results/"
FOLDER += "2023-09-01_17-19-30/"
FOLDER += "energy/"
FOLDER += "CIFAR10/"
FOLDER += "grid-search/"
FOLDER += "OptimalForOODClassSubjectToFNR/"
do_eval(FOLDER)



